import Vue from 'vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/all.css'
import { VueIocPlugin } from '@vue-ioc/core'

import App from './app/App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.use(VueIocPlugin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
