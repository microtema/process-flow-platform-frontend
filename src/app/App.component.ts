import Vue from 'vue'
import Component from 'vue-class-component'
import { Definition, DefinitionDiagramView, Home } from '@/views'
import { Module } from '@vue-ioc/core'
import ProcessDefinitionApiGateway
  from '@/components/process-definition/ProcessDefinition.api-gateway'

@Module({
  providers: [ProcessDefinitionApiGateway]
})
@Component({
  components: {
    Home,
    Definition,
    DefinitionDiagramView
  }
})
export default class AppComponent extends Vue {
  login () {
    Vue.prototype.$auth.signInWithRedirect()
  }

  logout () {
    Vue.prototype.$auth.signOut()
  }
}
