import Home from './home/Home.vue'
import Definition from './definition/definition.vue'
import DefinitionDiagramView from './definition-diagram/definition-diagram-view.vue'
import Callback from './login/Callback.vue'

export { Home, Definition, DefinitionDiagramView, Callback }
