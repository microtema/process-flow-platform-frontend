import { Vue, Component } from 'vue-property-decorator'
import { ProcessDefinition } from '@/components'

@Component({
  components: {
    ProcessDefinition
  }
})
export default class DefinitionComponent extends Vue {}
