import { Vue, Component } from 'vue-property-decorator'
import { ProcessDefinitionDiagram } from '@/components'

@Component({
  components: {
    ProcessDefinitionDiagram
  }
})
export default class DefinitionDiagramViewComponent extends Vue {}
