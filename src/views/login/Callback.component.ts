import { Vue, Component } from 'vue-property-decorator'

@Component
export default class CallbackComponent extends Vue {
  mounted () {
    console.log('callback...')
  }
}
