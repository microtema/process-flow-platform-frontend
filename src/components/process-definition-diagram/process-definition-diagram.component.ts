import { Vue, Component } from 'vue-property-decorator'
import { Inject } from '@vue-ioc/core'
import BpmnViewer from 'bpmn-js'
import ProcessDefinitionApiGateway
  from '@/components/process-definition/ProcessDefinition.api-gateway'

@Component
export default class ProcessDefinitionDiagramComponent extends Vue {
  bpmnViewer: BpmnViewer;
  canvas: any;

  @Inject()
  apiGateway: ProcessDefinitionApiGateway

  mounted () {
    this.bpmnViewer = new BpmnViewer({ container: '#bpmn-modeler' })
    this.canvas = this.bpmnViewer.get('canvas')
    const { id } = this.$route.params

    this.apiGateway.getDefinition(id).subscribe(it => {
      this.bpmnViewer.importXML(it.diagram)
        .then(() => this.canvas.zoom('fit-viewport', true))
        .catch(e => console.log('Unable to load Diagram', e))
    })
  }
}
