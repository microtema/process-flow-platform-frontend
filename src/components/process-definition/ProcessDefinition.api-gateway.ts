import Axios from 'axios-observable'
import { map } from 'rxjs/operators'

import { Injectable } from '@vue-ioc/core'

@Injectable()
export default class ProcessDefinitionApiGateway {
  http: Axios = Axios.create({
    baseURL: '/api/rest'
  });

  query () {
    return this.http.get('/definition').pipe(map((it) => it.data))
  }

  getDefinition (id: string) {
    return this.http.get('/definition/' + id).pipe(map((it) => it.data))
  }
}
